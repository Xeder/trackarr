module gitlab.com/cloudb0x/trackarr

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/IncSW/go-bencode v0.0.0-20191006112700-dd0b375b2060
	github.com/Masterminds/semver/v3 v3.0.3
	github.com/ReneKroon/ttlcache v1.6.0
	github.com/antchfx/xmlquery v1.2.3
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/antonmedv/expr v1.7.0
	github.com/asdine/storm/v3 v3.1.0
	github.com/daaku/go.zipexe v1.0.1 // indirect
	github.com/docker/go-units v0.4.0
	github.com/foolin/goview v0.3.0
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/imroc/req v0.3.0
	github.com/jpillora/backoff v1.0.0
	github.com/json-iterator/go v1.1.9
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/labstack/echo/v4 v4.1.15
	github.com/lithammer/shortuuid/v3 v3.0.4
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/olahol/melody v0.0.0-20180227134253-7bd65910e5ab
	github.com/onsi/ginkgo v1.10.2 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.6.2
	github.com/thoj/go-ircevent v0.0.0-20190807115034-8e7ce4b5a1eb
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	go.uber.org/atomic v1.6.0
	golang.org/x/lint v0.0.0-20200130185559-910be7a94367 // indirect
	golang.org/x/tools v0.0.0-20200225022059-a0ec867d517c // indirect
	gopkg.in/ini.v1 v1.52.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
